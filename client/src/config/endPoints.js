const apiGateWayUrl =
  process.env.NODE_ENV === "development" ? "http://localhost:4000/api/v1" : "";

const endPoints = {
  signIn: `${apiGateWayUrl}/signIn`,
  register: `${apiGateWayUrl}/signUp`,
  createGlossary: `${apiGateWayUrl}/createGlossary`,
  updateGlossary: `${apiGateWayUrl}/updateGlossary`,
  findGlossary: `${apiGateWayUrl}/getGlossaryList`,
  deleteGlossary: `${apiGateWayUrl}/deleteGlossary`,
};

export default endPoints;
