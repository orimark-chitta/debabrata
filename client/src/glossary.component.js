import React from "react";
import { FaPen, FaTrash } from "react-icons/fa";
import axios from "axios";
import endPoints from "./config/endPoints";
import { useNavigate } from "react-router-dom";

export default function Glossary() {
  const [state, setState] = React.useState({
    isUpdate: false,
    term: "",
    definition: "",
  });

  const token = localStorage.getItem("token");
  const [glossaryList, setGlossaryList] = React.useState([]);
  const [filter, setFilter] = React.useState("");
  const navigate = useNavigate();

  const fetchGlossary = () => {
    if (!token) {
      navigate("/");
    } else {
      axios.post(endPoints.findGlossary, { token }).then((r) => {
        setGlossaryList(r.data.results ? r.data.results : []);
      });
    }
  };

  const handleEdit = (data) => {
    setState({
      isUpdate: true,
      term: data.term,
      definition: data.definition,
      glossary_id: data._id,
    });
  };

  const handleDelete = (data) => {
    axios
      .delete(endPoints.deleteGlossary, {
        headers: {
          Authorization: token,
        },
        data: {
          glossary_id: data._id,
          token,
        },
      })
      .then((r) =>
        setGlossaryList(glossaryList.filter((gl) => gl._id !== data._id))
      );
  };

  React.useEffect(() => {
    fetchGlossary();
  }, []);

  return (
    <div
      style={{
        padding: "50px 40px",
      }}
    >
      <form
        className="container2"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <h3 id="logo">Glossary Store</h3>

        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            marginBottom: 50,
            width: "100%",
            gap: 10,
          }}
        >
          <input
            type="text"
            id="Term"
            name="Term"
            placeholder="Type in your Glossary Term.."
            autocomplete="off"
            required
            value={state.term}
            onChange={(e) =>
              setState({
                ...state,
                term: e.target.value,
              })
            }
          />
          <input
            id="Definition"
            name="Definition"
            placeholder="Enter your Glossary Definition.."
            autocomplete="off"
            required
            value={state.definition}
            onChange={(e) =>
              setState({
                ...state,
                definition: e.target.value,
              })
            }
          />

          {state.isUpdate ? (
            <button
              style={{
                background: "#000",
                borderColor: "#000",
              }}
              type="submit"
              name="submit"
              onClick={(e) => {
                axios
                  .patch(endPoints.updateGlossary, {
                    ...state,
                    token,
                  })
                  .then((r) => {
                    fetchGlossary();
                    setState({
                      isUpdate: false,
                      term: "",
                      definition: "",
                    });
                  });
              }}
            >
              Update
            </button>
          ) : (
            <button
              type="submit"
              name="submit"
              onClick={(e) => {
                axios
                  .post(endPoints.createGlossary, { ...state, token })
                  .then((r) => {
                    setGlossaryList([...glossaryList, r.data.results]);
                    setState({
                      isUpdate: false,
                      term: "",
                      definition: "",
                    });
                  });
              }}
            >
              Add
            </button>
          )}
        </div>
      </form>
      <input
        placeholder="Filter Glossary"
        value={filter}
        onChange={(e) => setFilter(e.target.value)}
      />
      <table class="content-table">
        <thead>
          <tr>
            <th>Glossary ID</th>
            <th>Term</th>
            <th>Definition</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {glossaryList
            .filter(
              (d) =>
                d.term.toLowerCase().includes(filter.toLowerCase()) ||
                d.definition.toLowerCase().includes(filter.toLowerCase())
            )
            .sort((a, b) => a.term.localeCompare(b.term))
            .map((bo, idx) => (
              <tr className={idx % 2 === 0 ? "" : "active-row"} key={idx}>
                <td>{bo._id}</td>
                <td>{bo.term}</td>
                <td>{bo.definition}</td>
                <td>
                  <span
                    style={{
                      display: "flex",
                      gap: 15,
                      fontSize: "1.2rem",
                      color: "#000",
                    }}
                  >
                    <FaPen
                      style={{
                        cursor: "pointer",
                      }}
                      onClick={() => handleEdit(bo)}
                    />
                    <FaTrash
                      style={{
                        cursor: "pointer",
                      }}
                      onClick={() => handleDelete(bo)}
                    />
                  </span>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}
