import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoginRegister from "./Login&Register";
import Glossary from "./glossary.component";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<LoginRegister />} />
        <Route exact path="/glossary" element={<Glossary />} />
      </Routes>
    </Router>
  );
}

export default App;
