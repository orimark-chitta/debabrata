const GlossaryDataModel = require("../models/glossary.model");
const { errorResponse, successResponse } = require("../utils/response.utils");

exports.getGlossaryDataController = async (req, res) => {
  try {
    const glossaryData = await GlossaryDataModel.find({
      author_id: req.body.id,
    });
    res.status(200).send(successResponse(glossaryData));
  } catch (e) {
    res.status(400).send(errorResponse(e));
  }
};

exports.createGlossaryDataController = async (req, res) => {
  try {
    const glossaryData = await GlossaryDataModel.create({
      ...req.body,
      author_id: req.body.id,
    });
    res.status(200).send(successResponse(glossaryData));
  } catch (e) {
    console.log(e);
    res.status(400).send(errorResponse(e));
  }
};

exports.updateGlossaryDataController = async (req, res) => {
  try {
    const glossaryData = await GlossaryDataModel.findByIdAndUpdate(
      req.body.glossary_id,
      req.body,
      { new: true }
    );
    res.status(200).send(successResponse(glossaryData));
  } catch (e) {
    res.status(400).send(errorResponse(e));
  }
};

exports.deleteGlossaryDataController = async (req, res) => {
  try {
    const glossaryData = await GlossaryDataModel.findByIdAndDelete(
      req.body.glossary_id,
      { new: true }
    );
    res.status(200).send(successResponse(glossaryData));
  } catch (e) {
    console.log(e);
    res.status(400).send(errorResponse(e));
  }
};
