const mongoose = require("mongoose");

const GlossaryDataModel = mongoose.model(
  "GlossaryData",
  new mongoose.Schema({
    author_id: mongoose.Types.ObjectId,
    term: String,
    definition: String,
    updatedAt: {
      type: Date,
      default: Date.now(),
    },
    createdAt: {
      type: Date,
      default: Date.now(),
    },
  })
);

module.exports = GlossaryDataModel;
