const {
  deleteGlossaryDataController,
  updateGlossaryDataController,
  createGlossaryDataController,
  getGlossaryDataController,
} = require("../controllers/glossary.controller");
const { isAuthenticated } = require("../middlewares/auth.middleware");

module.exports = function (app) {
  app.post(
    "/api/v1/getGlossaryList",
    isAuthenticated,
    getGlossaryDataController
  );
  app.post(
    "/api/v1/createGlossary",
    isAuthenticated,
    createGlossaryDataController
  );
  app.patch(
    "/api/v1/updateGlossary",
    isAuthenticated,
    updateGlossaryDataController
  );
  app.delete(
    "/api/v1/deleteGlossary",
    isAuthenticated,
    deleteGlossaryDataController
  );
};
